# FB2 to LaTeX XSL template 

**ATTENTION!** This XSL template is unfifnished.

Whats elements processing does not work:

+ `<fb:style>` 
+ `<fb:image>`
+ `<fb:sequence>`

Also there is absent all other elements (except defined in XSL file), 
which had defined in [FB2 XSD Schema](http://gribuser.ru/xml/fictionbook/2.0/xsd/FictionBook2.xsd).

# How to use

Because this stylesheet is XSL version 2.0 you must use Michael Key's parser [Saxon](http://saxon.sourceforge.net/). 
I prefer to use its professional edition. On Windows, you can do smth like this:

`java -jar saxon9pe.jar -xsl:fb2_2latex.xsl file.fb2 -o:file.tex`