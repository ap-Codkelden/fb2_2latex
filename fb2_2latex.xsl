<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
	xmlns:xlink="http://www.w3.org/1999/xlink" 
	xmlns:fb="http://www.gribuser.ru/xml/fictionbook/2.0" 
	xmlns:xs="http://www.w3.org/2001/XMLSchema" 
	xmlns:functx="http://www.functx.com">
<xsl:output method="text" omit-xml-declaration="yes" indent="no" encoding="UTF-8"/>
<xsl:key name="note-link" match="fb:section" use="@id"/>
<xsl:function name="functx:repeat-string" as="xs:string">
  <xsl:param name="stringToRepeat" as="xs:string?"/> 
  <xsl:param name="count" as="xs:integer"/> 
  <xsl:sequence select="string-join((for $i in 1 to $count return $stringToRepeat),'') "/>
</xsl:function>
<xsl:template match="/*">
\documentclass[paper=A4,pagesize=pdftex,headinclude=on,footinclude=on,12pt]{scrbook}
\usepackage[utf8]{inputenc}
\usepackage{cmap}
\usepackage[T2A]{fontenc}
\usepackage[russian]{babel}
\usepackage{hyperref}
\author{<xsl:choose>
<xsl:when test="not(fb:description/fb:title-info/fb:author/fb:nickname)">
	<xsl:value-of select="fb:description/fb:title-info/fb:author/fb:first-name"/><xsl:text> </xsl:text>
</xsl:when>
<xsl:otherwise>
	<xsl:if test="fb:description/fb:title-info/fb:author/fb:first-name">
		<xsl:value-of select="fb:description/fb:title-info/fb:author/fb:first-name"/><xsl:text> </xsl:text>
	</xsl:if>
</xsl:otherwise>
</xsl:choose>
<xsl:if test="fb:description/fb:title-info/fb:author/fb:middle-name">
	<xsl:value-of select="fb:description/fb:title-info/fb:author/fb:middle-name"/><xsl:text> </xsl:text>
</xsl:if>
<xsl:choose>
	<xsl:when test="not(fb:description/fb:title-info/fb:author/fb:nickname)">
		<xsl:value-of select="fb:description/fb:title-info/fb:author/fb:first-name"/><xsl:text> </xsl:text>
	</xsl:when>
	<xsl:otherwise>
		<xsl:if test="fb:description/fb:title-info/fb:author/fb:last-name">
			<xsl:value-of select="fb:description/fb:title-info/fb:author/fb:last-name"/><xsl:text> </xsl:text>
		</xsl:if>
	</xsl:otherwise>
</xsl:choose>}
\title{<xsl:value-of select="fb:description/fb:title-info/fb:book-title"/>}
<xsl:if test="fb:description/fb:title-info/fb:author/fb:email/text()!=''">
	<xsl:text>\thanks{</xsl:text><xsl:value-of select="fb:description/fb:title-info/fb:author/fb:email"/>}
</xsl:if>
\date{}

\begin{document}
\maketitle
<!--
	Аннотации
-->
<xsl:for-each select="fb:description/fb:title-info/fb:annotation">
	<xsl:call-template name="annotation"/>
</xsl:for-each>
<!-- 
	BUILD BOOK 
-->
<xsl:for-each select="fb:body">
<xsl:if test="position()!=1 or position()!=last()">
\clearpage
</xsl:if>
<xsl:if test="not(@name)">
	<xsl:apply-templates/>
</xsl:if>
<xsl:if test="@name!='notes'">
	\begin{center}
	<xsl:value-of select="@name"/>
	\end{center}
	<xsl:apply-templates/>
</xsl:if>
</xsl:for-each>
\end{document}
</xsl:template>
<!-- 
	author template 
-->
<xsl:template name="author">
\author<xsl:value-of select="fb:first-name"/>
<xsl:text disable-output-escaping="no">\,</xsl:text>
<xsl:value-of select="fb:middle-name"/>
<xsl:text disable-output-escaping="no">\,</xsl:text>
<xsl:value-of select="fb:last-name"/>
<xsl:call-template name="linebreack"/>
</xsl:template>
<!-- 
	secuence template 
-->
<xsl:template name="sequence">
<li/>
<xsl:value-of select="@name"/>
<xsl:if test="@number">
<xsl:text disable-output-escaping="no">, #</xsl:text>
<xsl:value-of select="@number"/>
</xsl:if>
<xsl:if test="fb:sequence">
<ul>
<xsl:for-each select="fb:sequence">
<xsl:call-template name="sequence"/>
</xsl:for-each>
</ul>
</xsl:if>
</xsl:template>
<!-- 
	description
 -->
<xsl:template match="fb:description">
<xsl:apply-templates/>
</xsl:template>
<!-- body -->
<xsl:template match="fb:body">
	<xsl:apply-templates/>
</xsl:template>

<xsl:template match="fb:section">
<a name="TOC_{generate-id()}"/>
<xsl:if test="@id">
<xsl:element name="a">
<xsl:attribute name="name">
<xsl:value-of select="@id"/>
</xsl:attribute>
</xsl:element>
</xsl:if>
<xsl:apply-templates/>
</xsl:template>
<!-- 
	section/title 
-->
<xsl:template match="fb:section/fb:title|fb:poem/fb:title">
	<xsl:variable name="level">
		<xsl:value-of select="count(ancestor::*[name()='section'])"/>
	</xsl:variable>
	<xsl:choose>
		<xsl:when test="$level=1">
			<xsl:text>\chapter</xsl:text>{<xsl:apply-templates/>} 
		</xsl:when>
		<xsl:when test="$level=2">
		<xsl:text>\section</xsl:text>{<xsl:apply-templates/>} 
		</xsl:when>
		<xsl:when test="$level&gt;2 and $level&lt;5">
			<xsl:text>\</xsl:text><xsl:value-of select="functx:repeat-string('sub',  xs:integer(xs:integer($level) - 2))"/><xsl:text>section</xsl:text>{<xsl:apply-templates/>} 
		</xsl:when>
		<xsl:when test="$level=5">
		<xsl:text>\paragraph</xsl:text>{<xsl:apply-templates/>} 
		</xsl:when>
		<xsl:otherwise>
			<xsl:text>\subparagraph</xsl:text>{<xsl:apply-templates/>} 
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>
<!-- 
	BODY/title 
-->
<xsl:template match="fb:body/fb:title">
\chapter{<xsl:apply-templates/>}
</xsl:template>
<!-- 
	title/p
-->
	<!-- subtitle будет с отступами -->
	<xsl:template match="fb:subtitle">
	<xsl:if test="@id">
		<xsl:call-template name="hypertarget"/>
	</xsl:if>
	<h5>
	<xsl:apply-templates/>
	</h5>
</xsl:template>

<xsl:template match="fb:p/fb:emphasis/fb:strong">
	<xsl:apply-templates/>
</xsl:template>

<!-- 
	p
 -->
<xsl:template match="fb:p">
	<xsl:if test="@id">
		<xsl:call-template name="hypertarget"/>
	</xsl:if>
	<xsl:apply-templates/>
	<xsl:call-template name="linebreack"/>
</xsl:template>

<!-- 
	strong
-->
<xsl:template match="fb:strong">
	\textbf{<xsl:apply-templates/>}
</xsl:template>
<!-- 
	emphasis 
-->
<xsl:template match="fb:emphasis">
\textit{<xsl:apply-templates/>}
</xsl:template>
<!-- 
style 

<xsl:template match="fb:style">
<span class="{@name}">
<xsl:apply-templates/>
</span>
</xsl:template> -->
<!-- empty-line -->
<xsl:template match="fb:empty-line">
	<xsl:call-template name="linebreack"/>
</xsl:template>
<!-- 
	link
 -->
<xsl:template match="fb:a">
	<xsl:choose>
		<xsl:when test="starts-with(@xlink:href,'#') and @type='note'">
			\footnote{<xsl:value-of select="key('note-link',substring-after(@xlink:href,'#'))/fb:p"/>}
		</xsl:when>
		<xsl:when test="@xlink:href and @type!='note'">
			\href{<xsl:value-of select="@xlink:href"/>}{<xsl:value-of select="text()"/>}
		</xsl:when>
		<xsl:when test="@xlink:href and not(@type)">
			\href{<xsl:value-of select="@xlink:href"/>}{<xsl:value-of select="text()"/>}
		</xsl:when>
		<xsl:when test="starts-with(@xlink:href,'#')">
			\hypertarget{<xsl:value-of select="@xlink:href"/>}{<xsl:value-of select="text()"/>}
		</xsl:when>
		<xsl:otherwise>
		\hypertarget{<xsl:value-of select="@xlink:id"/>}{<xsl:value-of select="text()"/>}
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<!-- annotation -->
<xsl:template name="annotation">
\chapter*{\centering \begin{normalsize}Abstract\end{normalsize}}
\begin{quotation}
\noindent<xsl:apply-templates/>
\end{quotation}
\clearpage
</xsl:template>
<!-- 
	epigraph 
-->
<xsl:template match="fb:epigraph">
\hfill \begin{minipage}[h]{0.45\textwidth}
<xsl:apply-templates/>
\end{minipage}
</xsl:template>
<!-- 
	epigraph/text-author 
-->
<xsl:template match="fb:epigraph/fb:text-author">
\hfill \begin{minipage}[h]{0.45\textwidth}
\begin{flushright}
<xsl:apply-templates/>
\end{flushright}
\end{minipage}
</xsl:template>
<!--
	cite 
-->
<xsl:template match="fb:cite">
\begin{quote}
<xsl:apply-templates/>
\end{quote}
</xsl:template>

<!-- cite/text-author -->
<xsl:template match="fb:text-author">
\begin{quote}
\begin{flushright}
<xsl:apply-templates/>
\end{flushright}
\end{quote}
</xsl:template>
<!-- 
	date
 -->
<xsl:template match="fb:date">
<xsl:choose>
<xsl:when test="not(@value)">
<xsl:apply-templates/>
<br/>
</xsl:when>
<xsl:otherwise>
<xsl:value-of select="@value"/>
<br/>
</xsl:otherwise>
</xsl:choose>
</xsl:template>
<!-- 
	poem
 -->
<xsl:template match="fb:poem">
<xsl:apply-templates/>
</xsl:template>
<!-- 
	stanza 
-->
<xsl:template match="fb:stanza">
\begin{verse}
<xsl:apply-templates/>
\end{verse}
</xsl:template>
<!-- 
	v
 -->
<xsl:template match="fb:v">
	<xsl:apply-templates/>\\
</xsl:template>

<xsl:template match="fb:code">
	\texttt{<xsl:apply-templates/>}
</xsl:template>

<xsl:template name="linebreack">
	<xsl:text>&#xa;</xsl:text>
</xsl:template>

<xsl:template name="hypertarget">
\hypertarget{<xsl:value-of select="@id"/>}
</xsl:template>

<!-- 
	image
 -->
<!-- <xsl:template match="fb:image">
<div align="center">
<img border="1">
<xsl:choose>
<xsl:when test="starts-with(@xlink:href,'#')">
<xsl:attribute name="src">
<xsl:value-of select="substring-after(@xlink:href,'#')"/>
</xsl:attribute>
</xsl:when>
<xsl:otherwise>
<xsl:attribute name="src">
<xsl:value-of select="@xlink:href"/>
</xsl:attribute>
</xsl:otherwise>
</xsl:choose>
</img>
</div>
</xsl:template> -->
</xsl:stylesheet>